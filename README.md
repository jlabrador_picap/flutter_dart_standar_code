A continuación se definen las bases de código para cada uno de los proyectos que se trabajen al interior de la empresa.

**La arquitectura MVVM basada en stacked** es la base actual en la empresa, y estamos en la tarea de migrar todos los proyectos a esta arquitectura y que los nuevos proyectos se basen en está.

Por tanto, espero este documento los ayude a **conocer, comprender, orientarse y también a respetar estás reglas**.

Mi nombre es **Juan Labrador**, cualquier duda, comentario o sugerencia puedes escribirme.

Este documento puede estar **sujeto a cambios**, igual en la medida de lo posible, si hay un cambio significativo les aviso para que lo revisen en los archivos. Muchas gracias.